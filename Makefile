# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/23 13:34:29 by ollevyts          #+#    #+#              #
#    Updated: 2018/03/23 13:34:31 by ollevyts         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all, compile, clean, fclean, re

NAME = fdf
SRC = fdf.c pix_mid.c spin.c key_button.c draw.c 
INC_LIBFT = ./libft
CC = gcc
FLAG = -Wall -Wextra -Werror 
MLX_LINK = -lmlx -framework OpenGL -framework AppKit -L libft/ -lft
OBJ = $(subst .c, .o, $(SRC))

all: compile

compile:
	make -C ./libft/
	@$(CC) $(FLAG) -o $(NAME) $(SRC) -I $(INC_LIBFT) $(MLX_LINK)
	@echo "Compiling" [ $(NAME) ]

clean:
	make clean -C libft/
	rm -f $(OBJ)

fclean:
	make fclean -C libft/
	rm -f $(NAME)

re: fclean all
