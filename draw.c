/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 18:38:44 by ollevyts          #+#    #+#             */
/*   Updated: 2018/03/23 18:38:45 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void ft_if(int *cords, int f, t_cord cord, t_mlx *mlx)
{
	int	h;
	int	w;

	h = cords[0];
	w = cords[1];
	if (f == 0)
	{
		if (w + 1 < mlx->mx)
			{
				cord.x2 = mlx->pnt[h][w + 1].npx;
				cord.y2 = mlx->pnt[h][w + 1].npy;
				cord.col2 = mlx->pnt[h][w + 1].col;
				line(mlx, cord);
			}
	}
	else
	{
			if (h + 1 < mlx->my)
			{
				cord.x2 = mlx->pnt[h + 1][w].npx;
				cord.y2 = mlx->pnt[h + 1][w].npy;
				cord.col2 = mlx->pnt[h + 1][w].col;
				line(mlx, cord);
			}		
	}
}

static void	lines(t_mlx *mlx, int f)
{
	int	cords[2];
	t_cord	cord;

	cords[0] = 0;
	while (cords[0] < mlx->my)
	{
		cords[1] = 0;
		while (cords[1] < mlx->mx)
		{
			cord.x1 = mlx->pnt[cords[0]][cords[1]].npx;
			cord.y1 = mlx->pnt[cords[0]][cords[1]].npy;
			cord.col1 = mlx->pnt[cords[0]][cords[1]].col;
			ft_if(cords, f, cord, mlx);
			cords[1]++;
		}
		cords[0]++;
	}
}

static void	lines_x(t_mlx *mlx)
{
	lines(mlx, 0);
}

static void	lines_y(t_mlx *mlx)
{
	lines(mlx, 1);
}

int			draw(t_mlx *mlx)
{
	sz_fig(mlx, 0);
	sz_fig(mlx, 1);
	spin_x(mlx);
	spin_y(mlx);
	spin_z(mlx);
	mid(mlx);
	lines_x(mlx);
	lines_y(mlx);
	if (mlx->mx == 1 && mlx->my == 1)
		pix(CEN + mlx->off_x, CEN + mlx->off_y, mlx->pnt[0][0].col, mlx);
	mlx_put_image_to_window(mlx->mi, mlx->mw, mlx->img, 3, 3);
	mlx_destroy_image(mlx->mi, mlx->img);
	mlx->img = mlx_new_image(mlx->mi, 1500, 1500);
	legend(mlx);
	if (mlx->anim == 1)
		mlx->vy += 1;
	return (0);
}
