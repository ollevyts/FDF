/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 18:38:23 by ollevyts          #+#    #+#             */
/*   Updated: 2018/03/23 18:38:25 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		coord(t_mlx *mlx, int i, char *split)
{
	mlx->pnt[mlx->my][i].px = i;
	mlx->pnt[mlx->my][i].py = mlx->my;
	mlx->pnt[mlx->my][i].z = ft_atoi(split);
}

static void	wr_pnt(t_mlx *mlx, int i, char *split)
{
	coord(mlx, i, split);
	if (ft_strchr(split, ','))
		mlx->pnt[mlx->my][i].col =
			ft_atoi_hex(ft_strchr(split, ',') + 1);
	else
		mlx->pnt[mlx->my][i].col = (mlx->pnt[mlx->my][i].z > 0) 
													? 0x3700ff : 0xff0000;
}

static void	sum_x(t_mlx *mlx, char *line)
{
	int	x;
	char		**split;
	int			i;

	i = 0;
	x = 0;
	split = ft_strsplit(line, ' ');
	while (split[++i] != 0)
		wr_pnt(mlx, i, split[i]);
	if (i < x || x == 0)
		x = i;
	mlx->mx = x;
	i = 0;
	while (split[++i] != 0)
		free(split[i]);
	free(split);
}

static void	ft_read(t_mlx *mlx, int argc, char **argv)
{
	int		fd;
	char	*line;

	if (argc < 2 || argc > 2)
		exit((int)(write(1, "Apply the source file!!!\n", 25)));
	if ((fd = open(argv[1], O_RDONLY)) == -1)
		exit((int)write(1, "ERROR\n", 6));
	mlx->my = 0;
	while (get_next_line(fd, &line) > 0)
	{
		sum_x(mlx, line);
		mlx->my++;
		free(line);
	}
	if ((fd = open(argv[1], O_RDONLY)) == -1)
		exit(write(1, "ERROR\n", 6));
}

int			main(int argc, char **argv)
{
	t_mlx *mlx;

	mlx = malloc(sizeof(t_mlx));
	if (!mlx)
		return (0);
	ft_read(mlx, argc, argv);
	int_mlx(mlx);
	mlx_hook(mlx->mw, 2, 5, my_key_funct, mlx);
	mlx_loop_hook(mlx->mi, draw, mlx);
	mlx_hook(mlx->mw, 17, 1L << 17, NULL, &mlx);
	mlx_loop(mlx->mi);
	free(mlx);
	return (0);
}
