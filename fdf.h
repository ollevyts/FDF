/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 13:35:52 by ollevyts          #+#    #+#             */
/*   Updated: 2018/03/23 13:35:54 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "mlx.h"
# include "libft.h"
# include <limits.h>
# include <math.h>
# include <unistd.h>
# include <stdio.h>
# define CEN 1500/2
# define PI_ANG 3.14159265359 / 180
# define SIN(value) sin(value * PI_ANG)
# define COS(value) cos(value * PI_ANG)

typedef	struct		s_point
{
	int				px;
	int				py;
	int				z;
	int				npx;
	int				npy;
	int				npz;
	int				col;
}					t_point;

typedef	struct		s_cord
{
	int				x1;
	int				y1;
	int				x2;
	int				y2;
	int				col1;
	int				col2;
}					t_cord;

typedef	struct		s_mlx
{
	void			*mi;
	void			*mw;
	void			*img;
	int				dx;
	int				dy;
	int				sx;
	int				sy;
	int				err;
	int				err2;
	int				off_x;
	int				off_y;
	int				mx;
	int				my;
	int				vx;
	int				vy;
	int				vz;
	char			*data;
	int				sl;
	int				bp;
	int				anim;
	int				help;
	float			set;
	double			rise;
	t_point			pnt[2000][2000];
}					t_mlx;

void				int_mlx(t_mlx *mlx);
void				mid(t_mlx *mlx);
void				pix(int x, int y, int color, t_mlx *mlx);
void				spin_y(t_mlx *mlx);
void				spin_x(t_mlx *mlx);
void				spin_z(t_mlx *mlx);
void				sz_fig(t_mlx *mlx, int i);
int					my_key_funct(int key, t_mlx *mlx);
int					draw(t_mlx *mlx);
void				legend(t_mlx *mlx);
void				line(t_mlx *mlx, t_cord cord);

#endif
