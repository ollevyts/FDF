/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_button.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 18:39:06 by ollevyts          #+#    #+#             */
/*   Updated: 2018/03/23 18:39:07 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		legend(t_mlx *mlx)
{
	if (mlx->help == 0)
	{
		mlx_string_put(mlx->mi, mlx->mw, 1050, 10, 0xf44141,
						"Animation: A");
		mlx_string_put(mlx->mi, mlx->mw, 1050, 25, 0xf44141,
						"Help: TAB");
		mlx_string_put(mlx->mi, mlx->mw, 1050, 40, 0xf44141,
						"Rotate: x -'4', '6' y - '2' '8'");
		mlx_string_put(mlx->mi, mlx->mw, 1050, 55, 0xf44141,
						"Size map: + and - on numpad");
		mlx_string_put(mlx->mi, mlx->mw, 1050, 70, 0xf44141,
						"Scale: / and * on numpad");
		mlx_string_put(mlx->mi, mlx->mw, 1050, 85, 0xf44141,
						"Position: arrows");
	}
}

static void	my_key_pt2(int key, t_mlx *mlx)
{
	if (key == 91 || key == 28)
		mlx->vx += 3;
	if (key == 67)
		mlx->rise *= 1.1;
	if (key == 75)
		mlx->rise *= 0.9;
	if (key == 126)
		mlx->off_y -= 3;
	if (key == 125)
		mlx->off_y += 3;
	if (key == 123)
		mlx->off_x -= 3;
	if (key == 124)
		mlx->off_x += 3;
	if (key == 48)
		mlx->help = (mlx->help == 0) ? 1 : 0;
	if (key == 0)
		mlx->anim = (mlx->anim == 0) ? 1 : 0;
	draw(mlx);
}

int			my_key_funct(int key, t_mlx *mlx)
{
	if (key == 53)
		exit(0);
	if (key == 69)
	{
		mlx->rise *= 1.1;
		mlx->set *= 1.2;
	}
	if (key == 78 && mlx->set > 0)
	{
		mlx->set *= 0.8;
		mlx->rise *= 0.9;
	}
	if (key == 83 || key == 18)
		mlx->vz += 3;
	if (key == 92 || key == 19)
		mlx->vz -= 3;
	if (key == 86 || key == 21)
		mlx->vy += 3;
	if (key == 88 || key == 23)
		mlx->vy -= 3;
	if (key == 84 || key == 26)
		mlx->vx -= 3;
	my_key_pt2(key, mlx);
	return (0);
}
