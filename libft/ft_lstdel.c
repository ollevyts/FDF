/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 07:15:03 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/11 07:15:07 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	if (*alst == NULL || del == NULL)
		return ;
	if ((*alst)->next == NULL)
	{
		ft_lstdelone(alst, del);
		return ;
	}
	ft_lstdel(&(*alst)->next, del);
	ft_lstdelone(alst, del);
}
