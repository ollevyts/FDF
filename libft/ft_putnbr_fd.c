/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <ollevyts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 20:20:01 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/03 22:15:35 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long long z;

	z = (long long)n;
	if (z < 0)
	{
		ft_putchar_fd('-', fd);
		z = -z;
	}
	if (z > 9)
	{
		ft_putnbr_fd(z / 10, fd);
		ft_putnbr_fd(z % 10, fd);
	}
	if (z < 10)
		ft_putchar_fd(z + '0', fd);
}
