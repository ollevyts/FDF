/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 17:00:56 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/01 17:01:00 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	char	*mal;
	size_t	j;

	i = 0;
	if (s == NULL || f == NULL)
		return (NULL);
	j = ft_strlen(s);
	mal = (char *)malloc(sizeof(char) * (j + 1));
	if (mal == NULL)
		return (NULL);
	while (s[i])
	{
		mal[i] = f(s[i]);
		i++;
	}
	mal[i] = '\0';
	return (mal);
}
