/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 17:15:51 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/01 17:15:54 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	char	*mal;
	size_t	j;

	i = 0;
	if (s == NULL || f == NULL)
		return (NULL);
	j = ft_strlen(s);
	mal = (char *)malloc(sizeof(char) * (j + 1));
	if (mal == NULL || s == NULL || f == NULL)
		return (NULL);
	while (s[i])
	{
		mal[i] = f(i, s[i]);
		i++;
	}
	mal[i] = '\0';
	return (mal);
}
