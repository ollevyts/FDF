/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 15:15:12 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/07 15:15:14 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_line	*get_fd(t_line **lin, int fd)
{
	t_line	*temp;

	temp = *lin;
	while (temp)
	{
		if (temp->fd == fd)
		{
			if (!(temp->str))
				temp->str = ft_strnew(0);
			return (temp);
		}
		temp = temp->next;
	}
	if (!(temp = malloc(sizeof(*temp))))
		return (NULL);
	temp->fd = fd;
	temp->str = ft_strnew(0);
	temp->next = *lin;
	*lin = temp;
	return (temp);
}

static int		ft_gnl(t_line *temp, char **line)
{
	char	*tmp;
	char	*pnt;

	if (temp->str)
	{
		if (!(pnt = ft_strchr(temp->str, 10)))
		{
			*line = temp->str;
			temp->str = NULL;
		}
		else
		{
			*line = ft_strnew(pnt - temp->str);
			ft_memcpy(*line, temp->str, pnt - temp->str);
			tmp = temp->str;
			temp->str = ft_strdup(++pnt);
			free(tmp);
		}
		return (1);
	}
	return (0);
}

int				ft_reading(int fd, t_line *temp)
{
	char	buf[BUFF_SIZE + 1];
	char	*tmp;
	int		ret;

	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = 0;
		tmp = temp->str;
		if (!(temp->str = ft_strjoin(temp->str, buf)))
			return (-1);
		free(tmp);
		if (ft_strchr(temp->str, 10))
			break ;
	}
	if (ret == -1)
		return (-1);
	if (ft_strlen(temp->str) == 0)
		return (0);
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	static	t_line	*lin;
	t_line			*temp;
	int				read;
	int				ret;

	if (fd < 0 || !line)
		return (-1);
	temp = get_fd(&lin, fd);
	if (!(ft_strchr(temp->str, 10)))
	{
		read = ft_reading(fd, temp);
		if (read == -1)
			return (-1);
		if (read == 0)
			return (0);
	}
	ret = ft_gnl(temp, line);
	return ((ret > 0) ? 1 : 0);
}
