/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pix_mid.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 18:39:43 by ollevyts          #+#    #+#             */
/*   Updated: 2018/03/23 18:39:45 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
void	int_mlx(t_mlx *mlx)
{
	int		end;

	mlx->mi = mlx_init();
	mlx->mw = mlx_new_window(mlx->mi, 1500, 1500, "FDF");
	mlx->img = mlx_new_image(mlx->mi, 1500, 1500);
	mlx->data = mlx_get_data_addr(mlx->img, &(mlx->bp),
			&(mlx->sl), &(end));
	mlx->off_x = 0;
	mlx->off_y = 0;
	mlx->rise = 5;
	mlx->anim = 0;
	mlx->help = 0;	
	mlx->set = 30;
	mlx->vx = 270;
	mlx->vy = 90;
	mlx->vz = 0;
}
void	mid(t_mlx *mlx)
{
	int x;
	int y;

	x = 0;
	while (x < mlx->my)
	{
		y = 0;
		while (y < mlx->mx)
		{
			mlx->pnt[x][y].npx = mlx->pnt[x][y].npx + CEN + mlx->off_x;
			mlx->pnt[x][y].npy = mlx->pnt[x][y].npy + CEN + mlx->off_y;
			y++;
		}
		x++;
	}
}

void	pix(int x, int y, int col, t_mlx *mlx)
{
	if (x < 1500 && y < 1500 && x >= 0 && y >= 0)
	{
		mlx->data[(x * (mlx->bp / 8)) + (y * mlx->sl) + 0] = (char)(col & 0x0000FF);
		mlx->data[(x * (mlx->bp / 8)) + (y * mlx->sl) + 1] = (char)((col >> 8) & 0x0000FF);
		mlx->data[(x * (mlx->bp / 8)) + (y * mlx->sl) + 2] = (char)(col >> 16);
	}
}

static void	opt(t_mlx *mlx, t_cord cord)
{
	mlx->dx = abs(cord.x2 - cord.x1);
	mlx->dy = abs(cord.y2 - cord.y1);
	if (cord.x1 < cord.x2)
		mlx->sx = 1;
	else
		mlx->sx = -1;
	if (cord.y1 < cord.y2)
		mlx->sy = 1;
	else
		mlx->sy = -1;
	mlx->err = mlx->dx - mlx->dy;
}

void	line(t_mlx *mlx, t_cord cord)
{
	opt(mlx, cord);
	while (cord.x1 != cord.x2 || cord.y1 != cord.y2)
	{
		pix(cord.x1, cord.y1, cord.col1, mlx);
		mlx->err2 = mlx->err * 2;
		if (mlx->err2 > -mlx->dy)
		{
			mlx->err -= mlx->dy;
			cord.x1 += mlx->sx;
		}
		if (mlx->err2 < mlx->dx)
		{
			mlx->err += mlx->dx;
			cord.y1 += mlx->sy;
		}
	}
}



