/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spin.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 18:39:53 by ollevyts          #+#    #+#             */
/*   Updated: 2018/03/23 18:39:55 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	spin(t_mlx *mlx, int f, int a, int x)
{
	int	y;
	int i;
	int	t;

	while (x < mlx->my)
	{
		y = 0;
		while (y < mlx->mx)
		{

			i = (f == 0) ? mlx->pnt[x][y].z * mlx->rise : mlx->pnt[x][y].npz;
			t = (f == 0) ? mlx->pnt[x][y].npy : mlx->pnt[x][y].npx;
			if (f == 2)
			{
				t = mlx->pnt[x][y].npx;
				i = mlx->pnt[x][y].npy;
			}
			mlx->pnt[x][y].npy = (f == 2) ? round(t * SIN(a) + i * COS(a))
				: round(t * COS(a) + i * SIN(a));
			mlx->pnt[x][y].npz = round(-t * SIN(a) + i * COS(a));
			y++;
		}
		x++;
	}
}

void	spin_x(t_mlx *mlx)
{
	spin(mlx, 0, mlx->vx, 0);
}

void	spin_y(t_mlx *mlx)
{
	spin(mlx, 1, mlx->vy, 0);
}

void	spin_z(t_mlx *mlx)
{
	spin(mlx, 2, mlx->vz, 0);
}

void	sz_fig(t_mlx *mlx, int i)
{
	int		x;
	int		y;

	x = 0;
	while (x < mlx->my)
	{
		y = 0;
		while (y < mlx->mx)
		{
			if (i == 0)
			{
				mlx->pnt[x][y].npx = mlx->pnt[x][y].px * mlx->set;
				mlx->pnt[x][y].npy = mlx->pnt[x][y].py * mlx->set;
			}
			else
			{
				mlx->pnt[x][y].npx = mlx->pnt[x][y].npx - ((mlx->mx * mlx->set) / 2);
				mlx->pnt[x][y].npy = mlx->pnt[x][y].npy - ((mlx->my * mlx->set) / 2);
			}
			y++;
		}
		x++;
	}
}
